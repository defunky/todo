﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Todo.Models
{
    public class ManageUsersViewModel
    {
        public IdentityUser[] Admins { get; set; }
        public IdentityUser[] Users { get; set; }
    }
}
