﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Todo.Data;
using Todo.Models;

namespace Todo.Services
{
    public class TodoItemService : ITodoItemService
    {
        private readonly ApplicationDbContext m_context;

        public TodoItemService(ApplicationDbContext context)
        {
            m_context = context;
        }
        public async Task<TodoItem[]> GetIncompleteItemsAsync(IdentityUser user)
        {
            return await m_context.Items.Where(x => !x.Completed && x.UserId == user.Id).ToArrayAsync();
        }

        public async Task<bool> AddItemAsync(TodoItem item, IdentityUser user)
        {
            item.Id = Guid.NewGuid();
            item.UserId = user.Id;
            m_context.Items.Add(item);

            return await m_context.SaveChangesAsync() == 1;
        }

        public async Task<bool> MarkDoneAsync(Guid id, IdentityUser user)
        {
            var item = await m_context.Items.Where(x => x.Id == id && x.UserId == user.Id).SingleOrDefaultAsync();

            if (item == null) return false;

            item.Completed = true;

            return await m_context.SaveChangesAsync() == 1;
        }
    }
}
