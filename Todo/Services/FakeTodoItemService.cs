﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Todo.Models;

namespace Todo.Services
{
    public class FakeTodoItemService : ITodoItemService
    {
        public Task<TodoItem[]> GetIncompleteItemsAsync()
        {
            var t = new TodoItem()
            {
                Title = "Learning",
                DueAt = DateTimeOffset.Now.AddDays(1)
            };

            var t2 = new TodoItem()
            {
                Title = "Build",
                DueAt = DateTimeOffset.Now.AddDays(2)
            };

            return Task.FromResult(new[] { t, t2 });
}

        public Task<TodoItem[]> GetIncompleteItemsAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddItemAsync(TodoItem item, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MarkDoneAsync(Guid id, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddItemAsync(TodoItem item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MarkDoneAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
