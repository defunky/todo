﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualBasic;

namespace Todo
{
    public class SeedData
    {
        public static async Task InitalizeAsync(IServiceProvider services)
        {
            var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
            await EnsureRolesAsync(roleManager);

            var userManager = services.GetRequiredService<UserManager<IdentityUser>>();

            await EnsureTestAdminAsync(userManager);
        }

        private static async Task EnsureTestAdminAsync(UserManager<IdentityUser> userManager)
        {
            var test = userManager.Users.SingleOrDefault(x => x.UserName == "admin@a");

            if (test != null) return;

            test = new IdentityUser()
            {
                UserName = "admin@a",
                Email = "admin@a"
            };

            await userManager.CreateAsync(test, "admin");
            await userManager.AddToRoleAsync(test, "Administrator");
        }

        private static async Task EnsureRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var res = await roleManager.RoleExistsAsync("Administrator");

            if (res) return;

            await roleManager.CreateAsync(new IdentityRole("Administrator"));
        }
    }
}
