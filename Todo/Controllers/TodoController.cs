﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Todo.Models;
using Todo.Services;

namespace Todo.Controllers
{
    [Authorize]
    public class TodoController : Controller
    {
        private readonly ITodoItemService m_itemService;
        private readonly UserManager<IdentityUser> m_userManager;

        public TodoController(ITodoItemService itemService, UserManager<IdentityUser> userManager)
        {
            m_itemService = itemService;
            m_userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            var user = await m_userManager.GetUserAsync(User);

            var items = await m_itemService.GetIncompleteItemsAsync(user);
            var model = new TodoItemViewmodel
            {
                Items = items
            };

            return View(model);
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddItem(TodoItem item)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var user = await m_userManager.GetUserAsync(User);
            var res = await m_itemService.AddItemAsync(item, user);

            if (!res) return BadRequest("Could not add item");

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MarkDone(Guid id)
        {
            if (id == Guid.Empty)
            {
                return RedirectToAction("Index");
            }
            var user = await m_userManager.GetUserAsync(User);
            var res = await m_itemService.MarkDoneAsync(id, user);

            if (!res)
            {
                return BadRequest("Could not mark item as done");
            }

            return RedirectToAction("Index");
        }
    }
}