﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Todo.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Todo.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageUsersController : Controller
    {
        private readonly UserManager<IdentityUser> m_userManager;
        private readonly RoleManager<IdentityRole> m_roleManager;

        public ManageUsersController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            m_userManager = userManager;
            m_roleManager = roleManager;
        }  
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var admins = (await m_userManager.GetUsersInRoleAsync("Administrator")).ToArray();
            var users = await m_userManager.Users.ToArrayAsync();
            var model = new ManageUsersViewModel()
            {
                Admins = admins,
                Users = users
            };

            return View(model);
        }

        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = m_userManager.Users.FirstOrDefault(x => x.Id == id);

            var res = await m_userManager.DeleteAsync(user);

            if (!res.Succeeded) return BadRequest("Could not delete user");

            return RedirectToAction("Index");
        }
    }
}
